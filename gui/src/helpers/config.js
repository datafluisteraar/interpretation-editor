// icons per frame type
const icons = {
  'agent': 'mdi-account-switch',
  'object': 'mdi-account-arrow-left-outline',
  'action': 'mdi-gesture-tap',
  'duty': 'mdi-exclamation',
  'other': 'mdi-circle-small',
  'act': 'mdi-autorenew',
  'claim_duty': 'mdi-square',
}

// colors per frame type / frame subtype
const colors = {
  'fact': 'primary',
  'agent': 'warning',
  'object': 'accent',
  'action': 'secondary',
  'duty': 'info',
  'act': 'primary',
  'claim_duty': 'primary'

}

export {
  icons, colors
}
